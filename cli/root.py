import click

from core.combination import ALL_MODIFIERS, generate_wordlist


@click.group()
def root():
    pass


@root.command()
@click.option("-n", "--name", "name", required=True, help="company name")
@click.option(
    "-f", "--firstname", "firstnames", required=True, multiple=True, help="firstnames"
)
@click.option(
    "-m",
    "--modifier",
    "modifiers",
    required=True,
    multiple=True,
    type=click.Choice(list(ALL_MODIFIERS)),
    help="modifier",
)
def generate(name, firstnames, modifiers):
    res = generate_wordlist(name, firstnames, modifiers)
    click.echo(res)


if __name__ == "__main__":
    root()
