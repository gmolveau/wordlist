from click.testing import CliRunner

from cli.root import generate


def test_generate():
    runner = CliRunner()
    result = runner.invoke(
        generate,
        args=[
            "--name 'esna'",
            "--firstname 'greg'",
            "--modifier 'join_words'",
            "--modifier 'exclamation_mark'",
        ],
    )
    print(result.output)
    assert result.exit_code == 0
    assert result.output == """['esnagreg!', 'gregesna!']"""
