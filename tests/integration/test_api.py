import json

import pytest

from web.api import app as flask_app


def test_home():
    with flask_app.test_client() as test_client:
        response = test_client.get("/")
        assert response.status_code == 200


@pytest.mark.parametrize(
    "data,want",
    [
        (
            {
                "company": "esna",
                "firstnames": ["greg"],
                "modifiers": ["join_words", "exclamation_mark"],
            },
            ["esnagreg!", "gregesna!"],
        ),
        (
            {
                "company": "esna",
                "firstnames": ["greg", "xavier"],
                "modifiers": ["join_words", "exclamation_mark"],
            },
            ["esnagreg!", "gregesna!", "xavieresna!", "esnaxavier!"],
        ),
    ],
)
def test_post_generate_wordlist(data, want):
    with flask_app.test_client() as test_client:
        response = test_client.post(
            "/", data=json.dumps(data), content_type="application/json"
        )
        assert response.status_code == 200
        have = json.loads(response.data)
        assert sorted(want) == sorted(have)
