from core.combination import combine, exclamation_mark, generate_wordlist, join_words


def compare_lists(a, b):
    return sorted(a) == sorted(b)


def test_combine():
    company = "esna"
    firstnames = ["greg", "xavier"]
    want = [["esna", "greg"], ["esna", "xavier"]]
    have = combine(company_name=company, firstnames=firstnames)
    assert compare_lists(want, have)


def test_join_words():
    data = ["esna", "greg"]
    want = ["esnagreg", "gregesna"]
    have = join_words(data)
    assert compare_lists(want, have)


def test_exclamation_mark():
    data = ["esnagreg", "gregesna"]
    want = ["esnagreg!", "gregesna!"]
    have = exclamation_mark(data)
    assert compare_lists(want, have)


def test_generate_wordlist():
    company = "esna"
    firstnames = ["greg"]
    modifiers = ["join_words", "exclamation_mark"]
    want = ["esnagreg!", "gregesna!"]
    have = generate_wordlist(company, firstnames, modifiers)
    assert compare_lists(want, have)


def test_generate_wordlist_multiple():
    company = "esna"
    firstnames = ["greg", "xavier"]
    modifiers = ["join_words", "exclamation_mark"]
    want = ["esnagreg!", "gregesna!", "xavieresna!", "esnaxavier!"]
    have = generate_wordlist(company, firstnames, modifiers)
    assert compare_lists(want, have)
