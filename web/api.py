from flask import Flask, jsonify, request
from flask_expects_json import expects_json

from core.combination import generate_wordlist

app = Flask(__name__)


@app.get("/")
def hello_world():
    return "<p>Hello, World!</p>"


schema = {
    "type": "object",
    "properties": {
        "company": {"type": "string", "minLength": 1},
        "firstnames": {"type": "array", "items": {"type": "string"}},
        "modifiers": {"type": "array", "items": {"type": "string"}},
    },
    "required": ["company", "firstnames", "modifiers"],
}


@app.post("/")
@expects_json(schema)
def endpoint_generate_wordlist():
    data = request.get_json()
    wordlist = generate_wordlist(data["company"], data["firstnames"], data["modifiers"])
    return jsonify(wordlist)
