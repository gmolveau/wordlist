def combine(company_name: str, firstnames: list[str]) -> list[list[str]]:
    res = []
    for firstname in firstnames:
        res.append([company_name, firstname])
    return res


def join_words(words: list[str]) -> list[str]:
    res = []
    res.append(f"{words[0]}{words[1]}")
    res.append(f"{words[1]}{words[0]}")
    return res


def exclamation_mark(words: list[str]) -> list[str]:
    res = []
    for word in words:
        res.append(f"{word}!")
    return res


ALL_MODIFIERS = {
    "join_words": join_words,
    "exclamation_mark": exclamation_mark,
}


def generate_wordlist(company_name: str, firstnames: list[str], modifiers: list[str]):
    combinations = combine(company_name, firstnames)
    words = []
    for combination in combinations:
        output = []
        for i, modifier in enumerate(modifiers):
            if i == 0:
                output = ALL_MODIFIERS[modifier](combination)
            else:
                output = ALL_MODIFIERS[modifier](output)
        words += output
    return words
