SHELL := /bin/bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

CMD:= poetry run

.PHONY: help
.DEFAULT: help
help: ## show the usage
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk -F ':.*?## ' 'NF==2 {printf "%-35s %s\n", $$1, $$2}'

.PHONY: check
check: black-check ## check formatting/linting

.PHONY: black-check
black-check:
	$(CMD) black --check .

.PHONY: lint
lint: black mypy pylint pydocstyle bandit safety ## lint project

.PHONY: black
black:
	$(CMD) black

.PHONY: tests
tests: unit-tests ## run all tests

.PHONY: unit-tests
unit-tests: ## run unit tests
	$(CMD) python3 -m pytest tests/unit -v

.PHONY: integration-tests
integration-tests: ## run integration tests
	$(CMD) python3 -m pytest tests/integration -v

.PHONY: run
run: ## start the app
	$(CMD) flask --app web.api run --host=0.0.0.0

.PHONY: run-dev
run-dev: ## start the app
	FLASK_DEBUG=true $(CMD) flask --app web.api run